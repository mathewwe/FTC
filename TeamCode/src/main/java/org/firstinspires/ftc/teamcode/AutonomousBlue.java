package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

@Autonomous(name = "Autonomous: Blue Alliance")
public class AutonomousBlue extends AutonomousBaseNoCV {
    @Override
    public void loop(){
        //My team uses a switch statement because I prefer event based programming, and it is very simple to program 'if'
        //statements to do things such as detect sensor events, drive to certain thresholds, or meet certain conditions.

        //This autonomous uses a state machine to do the different things it does while being constantly looped.
        robot.boschBNO055.getIMUGyroAngles(robot.rollAngle, robot.pitchAngle, robot.yawAngle);
        switch (step){
            case 1:
                robot.launcher.setTargetPosition(launchTicks); //Launch the preloaded balls.
                robot.launcher.setPower(1);
                if(robot.launcher.getCurrentPosition() >= robot.launcher.getTargetPosition()){
                    step++;
                }
                break;
            case 2:
                robot.ball.setPosition(.5);
                if(robot.ball.getPosition() == 0){
                    step++;
                }
            case 3:
                waitInMilliseconds(800);
                step++;
                break;
            case 4:
                robot.launcher.setTargetPosition(launchTicks + 1680);
                robot.launcher.setPower(1);
                if(robot.launcher.getCurrentPosition() >= robot.launcher.getTargetPosition()){
                    step++;
                }
                break;
            case 5:
                driveLeft(5, .3);
                break;
            case 6:
                resetEncoders();
                break;
            case 7:
                waitInMilliseconds(100);
                step++;
                break;
            case 8:
                driveDiagonalLeft(31, 1);
                break;
            case 9:
                driveLeftUntil(.15, robot.ODS.getRawLightDetected() > 2);
                break;
            case 10:
                turnRightP(0, .5, .01, 1);
                break;
            case 11:
                resetEncoders();
                break;
            case 12:
                driveStraightUntil(DIR.FORWARD, .13, robot.Ultrasonic.cmUltrasonic() < 16);
                break;
            case 13:
                resetEncoders();
                break;
            case 14:
                driveLeft(1, .5);
                break;
            case 15:
                if(robot.beacon.red() >=2){
                    step = 16;
                }
                else{
                    step = 18;
                }
                break;
            case 16:
                driveRight(1.5, .5);
                break;
            case 17:
                resetEncoders();
                break;
            case 18:
                driveStraight(5, .5);
                break;
            case 19:
                resetEncoders();
            case 20:
                driveStraight(-7, .5);
                break;
            case 21:
                resetEncoders();
                break;
            case 22:
                turnRightP(4, .5, .01, 1);
                break;
            case 23:
                driveLeft(21, 1);
                break;
            case 24:
                driveLeftUntil(.15, robot.ODS.getRawLightDetected() > 2);
                break;
            case 25:
                turnRightP(0, .5, .01, 1);
                break;
            case 26:
                resetEncoders();
                break;
            case 27:
                driveStraightUntil(DIR.FORWARD, .13, robot.Ultrasonic.cmUltrasonic() < 15);
                break;
            case 28:
                resetEncoders();
                break;
            case 29:
                driveLeft(1, .5);
                break;
            case 30:
                resetEncoders();
                break;
            case 31:
                if(robot.beacon.red() >=2){
                    step = 32;
                }
                else{
                    step = 34;
                }
                break;
            case 32:
                driveRight(2.6, .5);
                break;
            case 33:
                resetEncoders();
                break;
            case 34:
                driveStraight(5, .5);
                break;
            case 35:
                resetEncoders();
                break;
            case 36:
                driveStraight(-10, 1);
                break;
            case 37:
                double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * 55 * 2;
                int roundedDistance = (int)Math.round(Distance);
                robot.motorFrontRight.setTargetPosition(roundedDistance);
                robot.motorRearLeft.setTargetPosition(roundedDistance);

                robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

                robot.motorFrontRight.setPower(1);
                robot.motorRearLeft.setPower(1);
                robot.motorFrontLeft.setPower(0);
                robot.motorRearRight.setPower(0);

                if(Math.abs(robot.motorFrontRight.getCurrentPosition()) >= Math.abs(robot.motorFrontRight.getTargetPosition())){
                    start = System.currentTimeMillis();
                    step++;
                }
                break;
            default:
                break;
        }
        AutoTelemetry();
    }
    @Override
    public void stop(){
        systemTime = System.nanoTime();
        Log.i("FtcRobotController", "IMU Stop method finished in: "
                + (-(systemTime - (systemTime = System.nanoTime()))) + " ns.");
    }
}
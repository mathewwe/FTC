package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by mathe on 12/11/2016.
 */

public class OffSeasonHW_T {

    public DcMotor motorFrontRight;
    public DcMotor motorFrontLeft;
    public DcMotor turntable;
    public DcMotor lift;
    public DcMotor launcher;
    //public Servo Elbow;
    //public Servo Wrist;
    HardwareMap HWMAP  = null;
    public OffSeasonHW_T(){

    }
    public void init(HardwareMap hwMap){
        HWMAP = hwMap;
        motorFrontLeft = HWMAP.dcMotor.get("FrontLeft");
        motorFrontRight = HWMAP.dcMotor.get("FrontRight");
        turntable = HWMAP.dcMotor.get("turntable");
        lift = HWMAP.dcMotor.get("lift");
        launcher = HWMAP.dcMotor.get("launcher");
        //Elbow = HWMAP.servo.get("Elbow");
        //Wrist = HWMAP.servo.get("Wrist");

        motorFrontLeft.setDirection(DcMotor.Direction.REVERSE);
        motorFrontRight.setDirection(DcMotor.Direction.FORWARD);

        motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        turntable.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        //Elbow.setPosition(.5);
        //Wrist.setPosition(.5);
    }

}

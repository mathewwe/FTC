package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.exception.RobotCoreException;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by mathe on 12/11/2016.
 */

public class OffSeasonHW_A {

    public DcMotor motorFrontRight;
    public DcMotor motorFrontLeft;
    public DcMotor lift;
    public DcMotor launcher;
    //public AdafruitIMU boschBNO055;
    public long systemTime;//Relevant values of System.nanoTime
    HardwareMap HWMAP  = null;
    public OffSeasonHW_A(){

    }
    public void init(HardwareMap hwMap){
        HWMAP = hwMap;
        motorFrontLeft = HWMAP.dcMotor.get("FrontLeft");
        motorFrontRight = HWMAP.dcMotor.get("FrontRight");
        lift = HWMAP.dcMotor.get("lift");
        launcher = HWMAP.dcMotor.get("launcher");
        systemTime = System.nanoTime();
        /*try {
            boschBNO055 = new AdafruitIMU(HWMAP, "bno055", (byte)(AdafruitIMU.BNO055_ADDRESS_A), (byte)AdafruitIMU.OPERATION_MODE_IMU);
        } catch (RobotCoreException e){
            Log.i("FtcRobotController", "Exception: " + e.getMessage());
        }
        Log.i("FtcRobotController", "IMU Init method finished in: "
                + (-(systemTime - (systemTime = System.nanoTime()))) + " ns.");*/

        motorFrontLeft.setDirection(DcMotor.Direction.FORWARD);
        motorFrontRight.setDirection(DcMotor.Direction.REVERSE);

        motorFrontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorFrontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }
    //volatile double[] rollAngle = new double[2], pitchAngle = new double[2], yawAngle = new double[2];

}


package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by mathe on 1/10/2017.
 */
@TeleOp(name = "Timer Test")
public class timer extends OpMode {
    double start;
    public double elapsedTime(){
        long now = System.currentTimeMillis();
        return (now-start)/1000;
    }
    @Override
    public void init(){
    }
    @Override
    public void start(){
        start = System.currentTimeMillis();
    }
    @Override
    public void loop(){
        telemetry.addData("Time", elapsedTime());
        if(elapsedTime() >= 5){
            start = System.currentTimeMillis();
        }
    }
}

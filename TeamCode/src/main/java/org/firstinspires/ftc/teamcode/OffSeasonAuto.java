package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by mathe_000 on 4/21/2017.
 */
@Autonomous(name = "OffSeason CV Test")
public class OffSeasonAuto extends OffSeasonAutoBase{
    @Override
    public void loop(){
        switch (step){
            case 1:
                driveStraightUntil(DIR.FORWARD, .1, CV.rawPose(CV.crowd) != null);
                break;
            case 2:
                driveStraightToCVPosition(15, .1, .001, CV.crowd);
                break;
            default:
                break;
        }
        AutoTelemetry();
    }
}

package org.firstinspires.ftc.teamcode;

import com.vuforia.HINT;
import com.vuforia.Vuforia;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by mathe_000 on 12/24/2016.
 */
public class ComputerVision {

    public VuforiaLocalizer.Parameters param = new VuforiaLocalizer.Parameters(R.id.cameraMonitorViewId);
    public VuforiaLocalizer vuforia;
    public VuforiaTrackables beacons;
    public VuforiaTrackables OffSeason;
    public VuforiaTrackable Wheels;
    public VuforiaTrackable Tools;
    public VuforiaTrackable Legos;
    public VuforiaTrackable Gears;
    public VuforiaTrackable crowd;
    public ComputerVision(){

    }
    public float getPose(final VuforiaTrackable obj, int index){
        OpenGLMatrix pose;
        VectorF translation;
        pose = ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
        if(pose != null) {
            translation = pose.getTranslation();
            return translation.get(index);
        }
        else{
            return 0; //MAKE SURE TO COMPENSATE.
        }
    }
    public double targetDistance(VuforiaTrackable obj){
        OpenGLMatrix pose;
        VectorF translation;
        double distance;
        pose = ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
        if(pose != null) {
            translation = pose.getTranslation();
            distance = Math.sqrt(translation.get(2) * translation.get(2) + translation.get(0) * translation.get(0));  // Pythagoras calc of hypotenuse
            return distance;
        }
        else{
            return 0; //MAKE SURE TO COMPENSATE.
        }
    }
    public double targetAngle(VuforiaTrackable obj){
        OpenGLMatrix pose;
        VectorF translation;
        double angle;
        pose = ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
        if(pose != null) {
            translation = pose.getTranslation();
            angle = Math.atan2(translation.get(2), translation.get(0)); // in radians
            return Math.toDegrees(angle);
        }
        else{
            return 0; //MAKE SURE TO COMPENSATE.
        }
    }
    public OpenGLMatrix rawPose(VuforiaTrackable obj){
        return ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
    }
    public void init(){
        param.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        param.vuforiaLicenseKey = "AcLL+AD/////AAAAGbjvuBpXskZYpWZYqpbntgFnVpdzgL/GCP21WABGLdYlHgkqlzsJQAiWS4zL6jxCebLU8fQdTA3WpFPxeVbl9OPFR2qcAHtHFII0ySxGjS2/0lqttS7mCiES2r2VkyalfQOcWmePNZamwmraJs/QqfAfkcW7TJsesMo/KYkHO3idc6xi6K0ZqsiC97kMps7ibmpAMQXr2I8VzdhO5250kna5dmzr5AGFzxjPSWb5lM/fckBXQZhFxt6AnBTBg9FV9iL1HwJm+t18xS4zuQTLXpZ5lALzZ5jKmxSPEt7rlwW2zrcscbM2TyzhOQXFYFvn083HJmIK2wXQqJHyJw89r5FSUOhVALUwtJjHuEib1QJv";
        param.cameraMonitorFeedback = VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES;
        Vuforia.setHint(HINT.HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 4);
        vuforia = ClassFactory.createVuforiaLocalizer(param);
        beacons = vuforia.loadTrackablesFromAsset("FTC_2016-17");
        OffSeason = vuforia.loadTrackablesFromAsset("FTC");
        Wheels = beacons.get(0);
        Tools = beacons.get(1);
        Legos = beacons.get(2);
        Gears = beacons.get(3);
        crowd = OffSeason.get(0);
        beacons.get(0).setName("Wheels");
        beacons.get(1).setName("Tools");
        beacons.get(2).setName("Legos");
        beacons.get(3).setName("Gears");
        OffSeason.get(0).setName("crowd");
    }
    public void start(){
        beacons.activate();
        OffSeason.activate();
    }
}

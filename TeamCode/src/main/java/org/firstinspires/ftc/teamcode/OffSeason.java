package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpModeRegistrar;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by mathe_000 on 3/3/2017.
 */
@TeleOp(name = "Off-Season Tele-Op")
public class OffSeason extends OpMode {
    public double normalize(double min, double max, double value){
        return (value - min)/(max-min);
    }
    boolean lastTime;
    static boolean changeMotorTargetOnButtonPress(boolean button, boolean lastButtonState, final DcMotor dcMotor, int incrementByAmount) {
        if (dcMotor == null) throw new NullPointerException("Launcher motor can't be null");
        if (!lastButtonState && button) {
            dcMotor.setTargetPosition(dcMotor.getTargetPosition() + incrementByAmount);
        }
        return button;
    }
    OffSeasonHW_T robot = new OffSeasonHW_T();
    double leftScaled;
    double rightScaled;
    @Override
    public void init(){
        robot.init(hardwareMap);
        robot.launcher.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }
    @Override
    public void start(){
        robot.launcher.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }
    @Override
    public void loop(){
        leftScaled = -gamepad1.left_stick_y * -gamepad1.left_stick_y * -gamepad1.left_stick_y;
        rightScaled = -gamepad1.right_stick_y * -gamepad1.right_stick_y * -gamepad1.right_stick_y;
        leftScaled = Range.clip(leftScaled, -1, 1);
        rightScaled = Range.clip(rightScaled, -1, 1);

        robot.motorFrontLeft.setPower(leftScaled);
        robot.motorFrontRight.setPower(rightScaled);
        if(gamepad2.left_bumper){
            robot.turntable.setPower(.5);
        }
        else if(gamepad2.right_bumper){
            robot.turntable.setPower(-.5);
        }
        else{
            robot.turntable.setPower(0);
        }

        if(gamepad2.dpad_up){
            robot.lift.setPower(-.5);
        }
        else if(gamepad2.dpad_down){
            robot.lift.setPower(.5);
        }
        else{
            robot.lift.setPower(0);
        }
        lastTime = changeMotorTargetOnButtonPress(gamepad1.x, lastTime, robot.launcher, 1680);
        robot.launcher.setPower(1);
        telemetry.addData(">", "Launcher Target: " + robot.launcher.getTargetPosition());
        telemetry.addData(">", "Launcher Position: " + robot.launcher.getCurrentPosition());
    }
}

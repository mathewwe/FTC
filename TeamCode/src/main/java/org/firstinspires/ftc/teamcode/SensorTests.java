package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;
/** * Created by mathe on 12/5/2016. */ @TeleOp(name = "Sensor Testing")
public class SensorTests extends AutonomousBaseNoCV {
    public double adjustAngle(double angle) {
        if (angle > 180) angle -= 360;
        if (angle <= -180) angle += 360;
        return angle;
    }
    double error;
    double power;
    public void turnP(double degrees, double speed, double kp) {
        double degInv = -degrees;
        error = adjustAngle(degInv - robot.yawAngle[1]);
        if (Math.abs(error) > 0.5) {
            power = kp * error;
            power = Range.clip(power, -speed, +speed);
            robot.motorFrontLeft.setPower(power);
            robot.motorRearLeft.setPower(power);
            robot.motorFrontRight.setPower(-power);
            robot.motorRearRight.setPower(-power);
        }
    }
    @Override
    public void loop(){
        robot.boschBNO055.getIMUGyroAngles(robot.rollAngle, robot.pitchAngle, robot.yawAngle);
        telemetry.addData("Gyro", robot.yawAngle[1]);
        telemetry.addData("Red", robot.beacon.red());
        telemetry.addData("Blue", robot.beacon.blue());
        telemetry.addData("ODS", robot.ODS.getRawLightDetected());
        telemetry.addData("Ultrasonic", robot.Ultrasonic.cmUltrasonic());

    }

}

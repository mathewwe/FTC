package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by mathe on 12/11/2016.
 */

public class TeleOpHardware {

    public DcMotor motorFrontRight;
    public DcMotor motorFrontLeft;
    public DcMotor motorRearLeft;
    public DcMotor motorRearRight;
    public DcMotor launcher;
    public DcMotor collector;
    public Servo ball;
    HardwareMap HWMAP  = null;
    public TeleOpHardware(){

    }
    public void init(HardwareMap hwMap){
        HWMAP = hwMap;
        motorFrontLeft = HWMAP.dcMotor.get("FrontLeft");
        motorFrontRight = HWMAP.dcMotor.get("FrontRight");
        motorRearLeft = HWMAP.dcMotor.get("RearLeft");
        motorRearRight = HWMAP.dcMotor.get("RearRight");
        launcher = HWMAP.dcMotor.get("launcher");
        collector = HWMAP.dcMotor.get("collector");
        ball = HWMAP.servo.get("ball");

        motorFrontLeft.setDirection(DcMotor.Direction.REVERSE);
        motorRearLeft.setDirection(DcMotor.Direction.FORWARD);
        motorFrontRight.setDirection(DcMotor.Direction.FORWARD);
        motorRearRight.setDirection(DcMotor.Direction.REVERSE);

        motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRearLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRearRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        launcher.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

}

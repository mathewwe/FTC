package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

/**
 * Created by mathe on 12/21/2016.
 */
// TODO: 2/14/2017 Make gyro methods use the same kind of timeout as the CV methods.
@Autonomous(name = "Autonomous: Red Alliance")
public class AutonomousRed extends AutonomousBaseNoCV {
    @Override
    public void loop(){
        //My team uses a switch statement because I prefer event based programming, and it is very simple to program 'if'
        //statements to do things such as detect sensor events, drive to certain thresholds, or meet certain conditions.

        //This autonomous uses a state machine to do the different things it does while being constantly looped.
        robot.boschBNO055.getIMUGyroAngles(robot.rollAngle, robot.pitchAngle, robot.yawAngle);
        switch (step){
            case 1:
                robot.launcher.setTargetPosition(launchTicks); //Launch the preloaded balls.
                robot.launcher.setPower(1);
                if(robot.launcher.getCurrentPosition() >= robot.launcher.getTargetPosition()){
                    step++;
                }
                break;
            case 2:
                robot.ball.setPosition(.5);
                if(robot.ball.getPosition() == 0){
                    step++;
                }
            case 3:
                waitInMilliseconds(800);
                step++;
                break;
            case 4:
                robot.launcher.setTargetPosition(launchTicks + 1680);
                robot.launcher.setPower(1);
                if(robot.launcher.getCurrentPosition() >= robot.launcher.getTargetPosition()){
                    step++;
                }
                break;
            case 5:
                driveLeft(5, .3);
                break;
            case 6:
                turnRightP(180, 1, .01, 5);
                break;
            case 7:
                resetEncoders();
                break;
            case 8:
                driveDiagonalRight(31, 1);
                break;
            case 9:
                resetEncoders();
                break;
            case 10:
                driveStraightUntil(DIR.FORWARD, .13, robot.Ultrasonic.cmUltrasonic() < 20);
                break;
            case 11:
                resetEncoders();
            case 12:
                driveRightUntil(.15, robot.ODS.getRawLightDetected() > 2);
                break;
            case 13:
                turnRightP(180, .5, .01, 1);
                break;
            case 14:
                resetEncoders();
                break;
            case 15:
                driveStraightUntil(DIR.FORWARD, .13, robot.Ultrasonic.cmUltrasonic() < 16);
                break;
            case 16:
                resetEncoders();
                break;
            case 17:
                driveLeft(1.7, .5);
                break;
            case 18:
                resetEncoders();
                break;
            case 19:
                if(robot.beacon.blue() ==0){
                    step = 21;
                }
                else{
                    step = 20;
                }
                break;
            case 20:
                driveRight(2, .5);
                break;
            case 21:
                resetEncoders();
                break;
            case 22:
                driveStraight(5, .5);
                break;
            case 23:
                resetEncoders();
            case 24:
                driveStraight(-5, .5);
                break;
            case 25:
                resetEncoders();
                break;
            case 26:
                driveRight(21, 1);
                break;
            case 27:
                driveRightUntil(.15, robot.ODS.getRawLightDetected() > 2);
                break;
            case 28:
                turnRightP(180, .5, .01, 1);
                break;
            case 29:
                resetEncoders();
                break;
            case 30:
                driveStraightUntil(DIR.FORWARD, .13, robot.Ultrasonic.cmUltrasonic() < 16);
                break;
            case 31:
                resetEncoders();
                break;
            case 32:
                driveLeft(1.7, .5);
                break;
            case 33:
                resetEncoders();
                break;
            case 34:
                if(robot.beacon.red() >=2){
                    step = 37;
                }
                else{
                    step = 35;
                }
                break;
            case 35:
                driveRight(2.6, .5);
                break;
            case 36:
                resetEncoders();
                break;
            case 37:
                driveStraight(5, .5);
                break;
            default:
                break;
        }
        AutoTelemetry();
    }
    @Override
    public void stop(){
        systemTime = System.nanoTime();
        Log.i("FtcRobotController", "IMU Stop method finished in: "
                + (-(systemTime - (systemTime = System.nanoTime()))) + " ns.");
    }
}

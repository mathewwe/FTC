package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpModeRegistrar;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import java.util.Locale;
@TeleOp(name = "Mecanum TeleOp")
public class MecanumTeleOp extends OpMode { //Not Finished

    private boolean xButtonWasPressedLastTimeThroughLoop = false;
    private boolean performingMotorServoSequence = false;

    private TeleOpHardware robot = new TeleOpHardware();
    @Override
    public void init() {
        robot.init(hardwareMap);
    }

    @Override
    public void start() {
        robot.launcher.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    @Override
    public void loop() {
        //Establish mecanum wheel values.
        float frontleft = -gamepad1.left_stick_x + gamepad1.left_stick_y - gamepad1.right_stick_x;
        float frontright = gamepad1.left_stick_x + gamepad1.left_stick_y + gamepad1.right_stick_x;
        float rearleft = gamepad1.left_stick_x + gamepad1.left_stick_y - gamepad1.right_stick_x;
        float rearright = -gamepad1.left_stick_x + gamepad1.left_stick_y + gamepad1.right_stick_x;

        //Clip values to +/-1.
        frontleft = Range.clip(frontleft, -1, 1);
        frontright = Range.clip(frontright, -1, 1);
        rearleft = Range.clip(rearleft, -1, 1);
        rearright = Range.clip(rearright, -1, 1);

        //Cube the input to scale motor power with a curve.
        frontleft = (float) scaleInput(frontleft);
        frontright = (float) scaleInput(frontright);
        rearleft = (float) scaleInput(rearleft);
        rearright = (float) scaleInput(rearright);

        robot.motorFrontLeft.setPower(frontleft);
        robot.motorFrontRight.setPower(frontright);
        robot.motorRearLeft.setPower(rearleft);
        robot.motorRearRight.setPower(rearright);

        //WARNING: FOLLOWING METHOD WILL CAUSE STUCK IN LOOP ERROR IF IT IS IMPEDED!!!! AVOID!

        if (gamepad1.x) { //Credit to: mlw72z on Reddit for suggestion. Thanks! This will perform launching and loading upon a single button press.
            if (!xButtonWasPressedLastTimeThroughLoop) {
                // We've just now pressed x for the first time
                robot.ball.setPosition(.5);
                //Set position of ball feeding servo to stop balls from entering the catapult. Works. Extend servo range to help loading
                robot.launcher.setPower(1); //Keep in mind: will overshoot if it has low battery.
                robot.launcher.setTargetPosition(robot.launcher.getCurrentPosition() + 1680);
                performingMotorServoSequence = true;
            }
            xButtonWasPressedLastTimeThroughLoop = true;
        }
        else xButtonWasPressedLastTimeThroughLoop = false;

        if (performingMotorServoSequence && Math.abs(robot.launcher.getCurrentPosition() - robot.launcher.getTargetPosition()) <= 5){
            // motor has reached target, time to move servo
            robot.ball.setPosition(0);
        }
        if (performingMotorServoSequence && (robot.ball.getPosition() == 0)){
            // Servo has also reached target
            performingMotorServoSequence = false; // we're done
        }
        if(gamepad1.right_bumper && !gamepad1.left_bumper){
            robot.collector.setPower(1); //Collection works very well.

        }
        if(gamepad1.left_bumper && !gamepad1.right_bumper){
            robot.collector.setPower(-1);
        }
        if(!gamepad1.right_bumper && !gamepad1.left_bumper){
            robot.collector.setPower(0);
        }

        //******TELEMETRY******

        telemetry.addData("1 ", "Front Left Power: " + String.format(Locale.US,"%.2f", frontleft));
        telemetry.addData("2 ", "Front Right Power: " + String.format(Locale.US,"%.2f", rearleft));
        telemetry.addData("3 ", "Rear Left Power: " + String.format(Locale.US,"%.2f", frontright));
        telemetry.addData("4 ", "Rear Right Power: " + String.format(Locale.US,"%.2f", rearright));
    }
    @Override
    public void stop(){
    }

    double scaleInput(double dVal)  {

        return  dVal * dVal * dVal;
    }

}
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

/**
 * Created by mathe_000 on 4/21/2017.
 */
@Autonomous(name = "RMI Gala CV")
public class RMIAuto extends OffSeasonAutoBase{
    @Override
    public void loop(){
        switch (step){
            case 1:
                robot.motorFrontLeft.setPower(driveStraightCVPower(200, .1, .001, CV.crowd) + turnCVPower(0, .05, .001, CV.crowd));
                robot.motorFrontRight.setPower(driveStraightCVPower(200, .1, .001, CV.crowd) - turnCVPower(0, .05, .001, CV.crowd));
                if(CV.rawPose(CV.crowd) != null) {
                    if(Math.abs(CV.targetDistance(CV.crowd) - 200) < 4 && elapsedTime() >= 1){
                        step++;
                    }
                    else if(Math.abs(CV.targetDistance(CV.crowd) - 200) < 4);
                    else{
                        start = System.currentTimeMillis();
                    }
                }
                break;
            default:
                break;
        }
        AutoTelemetry();
    }
}
